# Linux_Scripts

## Fix Random Freezes Linux Mint
* sudo su
* Enter your password
* wget -O - https://gitlab.com/voltrans-public-repos/linux_scripts/raw/master/issues/random_freezes_fix.sh | bash

## Install Brave Linux Mint
* sudo su
* Enter your password
* wget -O - https://gitlab.com/voltrans-public-repos/linux_scripts/raw/master/softwares/brave_linux_mint.sh | bash

## Enable ZSwap
* sudo su
* Enter your password
* wget -O - https://gitlab.com/voltrans-public-repos/linux_scripts/raw/master/features/zswap.sh | bash

## Create User 
* sudo su 
* Enter your password
* wget -O - https://gitlab.com/voltrans-public-repos/linux_scripts/raw/master/features/create-user.sh | bash
* Enter Hostname ( ComputerName ) : tên máy tính dán trên thùng máy 
* Enter Username ( Tên nhân viên ) : Tên-tiếng-Anh.Tên-tiếng-Việt-ký-tự-đầu-họ-và-tên .VD : Secrect Nguyễn Thanh Còn : secrect.conntt
* Enter 

## Install QLTS
* sudo su 
* Enter your password
* wget -O - https://gitlab.com/voltrans-public-repos/linux_scripts/raw/master/features/qlts.sh | bash
