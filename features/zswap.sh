#!/bin/bash
sudo sed -i "s|quiet splash|quiet splash zswap.enabled=1 zswap.compressor=lz4|g"  /etc/default/grub
sudo update-grub
sudo echo lz4 >> /etc/initramfs-tools/modules
sudo echo lz4_compress >> /etc/initramfs-tools/modules
update-initramfs -u
dmesg | grep -i zswap