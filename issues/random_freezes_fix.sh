sed -i '/GRUB_CMDLINE_LINUX=""/c\GRUB_CMDLINE_LINUX="intel_idle.max_cstate=1"' /etc/default/grub
update-grub

if grep -q "vm.swappiness=10" /etc/sysctl.conf; then
    echo "Swapiness Already Fixed"
else
    echo "Swapiness Fixed"
    echo "vm.swappiness=10" >> /etc/sysctl.conf
fi

echo "Freezes Error Fixed"
reboot